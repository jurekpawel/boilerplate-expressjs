const endpointsAccessConfig = require('./endpoints-access');
const createSecuredEndpoints = require('./endpoints-secure');

module.exports = {
	endpointsAccessConfig,
	createSecuredEndpoints,
};
