module.exports = {
	extends: [
		'airbnb-base',
		'plugin:jest/style',
	],
	env: {
		'jest/globals': true,
	},
	plugins: ['jest'],
	rules: {
		indent: ['error', 'tab'],
		'no-tabs': ['off'],
		'no-underscore-dangle': ['off'],
	},
};
